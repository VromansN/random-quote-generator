import requests


class Quote:
    @staticmethod
    def get_by_id(quote_id: int) -> dict:
        return requests.get(f'http://quotes.stormconsultancy.co.uk/quotes/{quote_id}.json').json()

    @staticmethod
    def get_random() -> dict:
        return requests.get('http://quotes.stormconsultancy.co.uk/random.json').json()

    @staticmethod
    def get_all() -> list:
        return requests.get('http://quotes.stormconsultancy.co.uk/quotes.json').json()

    @staticmethod
    def get_by_popularity() -> list:
        return requests.get('http://quotes.stormconsultancy.co.uk/popular.json').json()
