import os
import cherrypy
from jinja2 import Environment, FileSystemLoader
from webapp.app import WebApp

if __name__ == '__main__':
    env = Environment(loader=FileSystemLoader('templates'))
    config = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'cherrypy.conf')

    cherrypy.quickstart(root=WebApp(env), config=config)
