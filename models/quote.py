from __future__ import annotations
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base


class Quote(declarative_base()):
    __tablename__ = 'quotes'

    id = Column(Integer, primary_key=True)
    quote_id = Column(Integer)
    author = Column(String)
    quote = Column(String)
    permalink = Column(String)
    likes = Column(Integer)
    dislikes = Column(Integer)

    @staticmethod
    def create_from(data: dict) -> Quote:
        return Quote(quote_id=data.get('id'), author=data.get('author'), quote=data.get('quote'),
                     permalink=data.get('permalink'), likes=0, dislikes=0)

    def __iter__(self):
        yield 'id', self.id
        yield 'quote_id', self.quote_id
        yield 'author', self.author
        yield 'quote', self.quote
        yield 'permalink', self.permalink
        yield 'likes', self.likes
        yield 'dislikes', self.dislikes
