import json
import cherrypy
from distutils.util import strtobool
from jinja2 import Environment
from helpers.database import DatabaseManager as Database


class WebApp:
    def __init__(self, env: Environment) -> None:
        self.env = env
        Database(True)

    @cherrypy.expose
    def index(self) -> str:
        return self.env.get_template('pages/index.html').render()

    @cherrypy.expose
    def get_random_quote(self) -> str:
        return json.dumps(dict(Database().get_random_quote()))

    @cherrypy.expose
    def like_quote(self, quote_id: int, decrease: str = 'false') -> str:
        return json.dumps(dict(Database().like_quote(quote_id, bool(strtobool(decrease)))))
