#!/bin/bash

separator="-------------------------------------------"
root=$(pwd)
venv='venv'

function install_requirements() {
  cd $root
  source "$root/$venv/bin/activate"
  pip install --upgrade pip
  pip install -r requirements.txt
}

echo $separator
echo "Installing required packages..."
sudo apt install wget build-essential checkinstall -y
sudo apt install libreadline-gplv2-dev libncursesw5-dev libssl-dev sqlite3 libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev -y
echo "Done installing packages."

echo $saparator
echo "Downloading Python 3.9.4..."
sudo rm -rf /opt/Python-3.9.4.tgz
sudo wget https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tgz -P /opt
echo "Done downloading Python."

echo $separator
echo "Installing Python 3.9.4..."
cd /opt
sudo tar xzf Python-3.9.4.tgz
cd Python-3.9.4
sudo ./configure --enable-optimizations
sudo make altinstall
cd ../
sudo rm -rf Python-3.9.4*
echo "Python version installed: $(python3.9 -V) (It should read Python 3.9.4, install manually if an error occurred.)"

echo $separator
echo "Creating virtual environment..."
cd $root
python3.9 -m venv $venv

echo $separator
echo "Installing requirements..."
install_requirements
echo "Done installing requirements."

echo $separator
echo "Starting server... (press Ctrl + C to quit)"
python main.py

echo $separator
echo "All done!"
