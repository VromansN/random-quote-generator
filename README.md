# Random quote generator

<p align="center">
    <a href="https://gitlab.com/VromansN/random-quote-generator/" target="_blank">
       <img src="https://img.shields.io/badge/version-v1.0.0-blue?style=flat-square&logo=gitlab&link=https://gitlab.com/VromansN/random-quote-generator"/>
    </a>
    <a href="https://www.python.org/downloads/release/python-394/" target="_blank">
       <img src="https://img.shields.io/badge/python-v3.9.4-2b5b84?style=flat-square&logo=python&logoColor=FFDA6C&link=https://www.python.org/downloads/release/python-394/"/>
    </a>
    <a href="https://cherrypy.org/" target="_blank">
       <img src="https://img.shields.io/badge/CherryPy-v18.6.0-blue?style=flat-square&link=https://cherrypy.org/"/>
    </a>
    <a href="https://palletsprojects.com/p/jinja/" target="_blank">
       <img src="https://img.shields.io/badge/Jinja2-v2.11.3-blue?style=flat-square&link=https://palletsprojects.com/p/jinja/"/>
    </a>
    <a href="https://buefy.org/" target="_blank">
       <img src="https://img.shields.io/badge/Buefy-v0.9.6-blue?style=flat-square&link=https://buefy.org/"/>
    </a>
    <a href="https://zorinos.com/" target="_blank">
       <img src="https://img.shields.io/badge/Zorin_OS-v15.3-blue?style=flat-square&logo=zorin&logoColor=00A4EC&link=https://zorinos.com/"/>
    </a>
</p>

#### Description

A simple webserver, made with [CherryPy](https://cherrypy.org/), [Jinja2](https://palletsprojects.com/p/jinja/),
and [Buefy](https://buefy.org/) ([Vue.js](https://vuejs.org/) combined with [Bulma](https://bulma.io/)) which shows a
random (mostly IT-related) quote upon page load (refresh), or alternatively upon a button press. There's also an option
to like/dislike quotes, as wel as sharing a quote on various social media platforms. And there's a dark mode switch for
some extra flair.

## Project setup

If working on a Debian based distro, simply open up a terminal, navigate to the root of this project and
run `./setup.sh`. (You may need to add execute permission before running the script: `sudo chmod +x setup.sh`)\
Otherwise, follow these steps:

1. make sure you have [sqlite3](https://sqlite.org/index.html) installed on your machine (in case of errors you may need
   to re-install/-compile Python (from source))
    1. `sudo apt install sqlite3` (https://www.osradar.com/install-sqlite-ubuntu-20-04/)
    2. `sudo apt install libsqlite3-dev` (https://askubuntu.com/a/708694)
2. (re)install [Python 3.9.4](https://www.python.org/downloads/release/python-394/) (if needed)
3. create [virtual environment](https://docs.python.org/3/library/venv.html)
4. activate the virtual environment:
   ![activate venv](static/images/activate_python_venv.png "activate venv")
5. inside virtual environment, install requirements: `pip install -r requirements.txt`
6. **start server**: from root directory, run `python main.py`
7. browse to http://0.0.0.0:8080/ to start using the app

## Watching and compiling Sass

1. install [nodejs](https://nodejs.org/en/) (if needed), required for npm (node package manager)
2. install [sass](https://sass-lang.com/): `npm install -g sass`
3. watch for stylesheet changes: `sass -watch static/sass/style.sass static/css/style.css`
