import os
import sys
import random
from operator import itemgetter
from sqlalchemy import create_engine, Column, Integer, Text, MetaData, Table
from sqlalchemy.orm import declarative_base, sessionmaker, Session, DeclarativeMeta
from sqlalchemy.future import Connection
from sqlalchemy.exc import OperationalError
from models.quote import Quote as QuoteModel
from api.quote import Quote as QuoteApi


class DatabaseManager:
    DATABASE = 'db/database.db'

    def __init__(self, initialize: bool = False) -> None:
        try:
            if not os.path.exists('db'):
                os.makedirs('db')

            self._engine = create_engine(f'sqlite+pysqlite:///{self.DATABASE}', echo=True)
            self._base = declarative_base()
            self._base.metadata.create_all(self._engine)
            self._session = sessionmaker(bind=self._engine)()

            if initialize:
                self.initialize_database()
        except OperationalError as e:
            sys.exit(f'Something went wrong: "{e}".')

    def initialize_database(self) -> None:
        metadata = MetaData()
        Table(
            'quotes', metadata,
            Column('id', Integer, primary_key=True),
            Column('quote_id', Integer),
            Column('author', Text),
            Column('quote', Text),
            Column('permalink', Text),
            Column('likes', Integer),
            Column('dislikes', Integer),
        )
        metadata.create_all(self._engine)

        quote_db_count = self._session.query(QuoteModel).count()
        quotes_api = QuoteApi.get_all()
        """ Sort based on quote id, default returns in descending order and we want ascending order. """
        quotes_api = sorted(quotes_api, key=itemgetter('id'))

        if 0 == quote_db_count:
            self.insert_quotes(quotes_api, False)
        elif len(quotes_api) > quote_db_count:
            self.insert_quotes(quotes_api)

    def insert_quotes(self, quotes: list, insert_only_new: bool = True) -> None:
        quote_collection = []

        if insert_only_new:
            quotes_in_db = self._session.query(QuoteModel).all()

            for quote in quotes:
                """
                Check if quote from API isn't already present in the database (based on <quote_id>) and insert it if 
                not already present. 
                """
                matches = list(filter(lambda obj: obj.quote_id == quote.get('id'), quotes_in_db))

                if not matches:
                    quote_collection.append(QuoteModel.create_from(quote))
        else:
            for quote in quotes:
                quote_collection.append(QuoteModel.create_from(quote))

        self._session.bulk_save_objects(quote_collection)
        self._session.commit()

    def get_connection(self) -> Connection:
        return self._engine.connect()

    def get_session(self) -> Session:
        return self._session

    @staticmethod
    def get_base() -> DeclarativeMeta:
        return declarative_base()

    def get_random_quote(self) -> QuoteModel:
        rand = random.randrange(0, self._session.query(QuoteModel).count())

        return self._session.query(QuoteModel)[rand]

    def like_quote(self, quote_id: int, decrease: bool) -> QuoteModel:
        """
        - [ ] todo 1: make it so that a visitor can only increase/decrease the quote (dis)like once:
            - [ ] on first (dis)like: increase (dis)like by one
            - [ ] when selecting the other option (from like to dislike or vice versa): increase the new (dis)like by
            one and decrease the previous selection by one
            - [ ] use cookies and/or visitor Mac address (example: https://stackoverflow.com/a/22241056)
            - [ ] when (dis)liked a quote: make it visible to the user which one is selected
            - [ ] implement button disable while waiting for response
            - [ ] allow to unl(dis)like when same button pressed twice (so no longer a like, or dislike: neutral)
        - [ ] todo 2 (optional): don't allow guests to (dis)like, but instead force a login to be able to (dis)like
        """
        quote = self._session.query(QuoteModel).filter(QuoteModel.quote_id == quote_id).first()

        if decrease:
            quote.dislikes += 1
        else:
            quote.likes += 1

        self._session.commit()

        # have to get the quote again, since Session.commit() deletes the original object
        return self._session.query(QuoteModel).filter(QuoteModel.quote_id == quote_id).first()
